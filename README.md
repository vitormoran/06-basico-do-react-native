# React Native na prática

## Props

Os componentes podem ser personalizado quando eles são criados, com diferentes parâmetros. Esses parâmetros de criação são chamados de `props`.

Por exemplo, um componente básico do React Native é o `<Image>`. Quando você cria uma imagem, você pode usar um props chamado `source` para controlar qual imagem ela mostra.

`<Image source="https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg" style={{width: 193, height: 110}}/>`

## State

Existem dois tipos de dados que controlam um componente: `props` e `state`. props são definidos pelo pai e são fixos (apenas para leitura) durante todo o ciclo de vida de um componente. Para os dados que vão mudar, temos que usar o `state`.

Em geral, você deve inicializar o estado no construtor e, em seguida, chamar `setState` quando quiser alterá-lo.

`this.state = { isShowingText: true };`

`this.setState({ isShowingText: false })`

## Height and Width

### Fixed Dimensions

A maneira mais simples de definir as dimensões de um componente é adicionando uma largura e altura fixas ao estilo. Todas as dimensões no React Native são sem unidade e representam pixels.

``` jsx
<View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
<View style={{width: 100, height: 100, backgroundColor: 'skyblue'}} />
<View style={{width: 150, height: 150, backgroundColor: 'steelblue'}} />
```

### Flex Dimensions

Use `flex` no estilo de um componente para que o componente se expanda e diminua dinamicamente com base no espaço disponível. Normalmente você usará `flex: 1`, que diz ao componente para preencher todo o espaço disponível, compartilhado igualmente entre outros componentes com o mesmo pai. Quanto maior o valor do flex, maior a proporção de espaço que um componente terá em relação aos seus irmãos.

``` jsx
<View style={{flex: 1, backgroundColor: 'powderblue'}} />
<View style={{flex: 2, backgroundColor: 'skyblue'}} />
<View style={{flex: 3, backgroundColor: 'steelblue'}} />
```

## Layout with Flexbox

Um componente pode especificar o layout de seus filhos usando o `flexbox`. Ele foi projetado para fornecer um layout consistente em diferentes tamanhos de tela.

Você normalmente usará uma combinação de `flexDirection`, `alignItems` e `justifyContent` para obter o layout correto. São as propriedades mais utilizadas do `flex`.

- **flexDirection**
  - Adicionar `flexDirection` ao estilo de um componente determina o eixo principal de seu layout. Os elementos filhos podem ser organizados horizontalmente (linha) ou verticalmente (coluna). O padrão é coluna.
  - `<View style={{flex: 1, flexDirection: 'row'}}>`
- **alignItems**
  - Adicionar `justifyContent` ao estilo de um componente determina a distribuição de filhos ao longo do eixo primário. Os elementos podem ser distribuídas no início, no centro, no final ou espaçadas uniformemente. As opções disponíveis são `flex-start`, `center`, `flex-end`, `space-around`, `space-entre` e `space-evenly`
  - `<View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>`
- **justifyContent**
  - Adicionar alignItems ao estilo de um componente determina o alinhamento de filhos ao longo do eixo secundário (se o eixo primário for `row`, o secundário será `column` e vice-versa). Os elementos podem estar alinhados no início, no centro, no final ou esticadas para preencher. As opções disponíveis são `flex-start`, `center`, `flex-end` e `stretch`.

Essas propriedades são o básico, aqui o link de uma lista com todas elas: https://facebook.github.io/react-native/docs/layout-props

### Docs

- [Props](https://facebook.github.io/react-native/docs/props)
- [States](https://facebook.github.io/react-native/docs/states)
- [Style](https://facebook.github.io/react-native/docs/style)
- [Height and Width](https://facebook.github.io/react-native/docs/height-and-width)
- [Layout with Flexbox](https://facebook.github.io/react-native/docs/flexbox)

# Desafio

### Nesse desafio será permitido a execução em grupos, para validar o desenvolvimento dos dois integrantes os commits serão validados. Coloquem no nome do MR o nome dos integrantes e tambem no nome da branch: integrante-um/integrante-dois

O desafio será a criação de um Filtro de Pedidos. Há um JSON na pasta do desafio que vocês deverão utilizar para poder concluir o desafio

Para concluir o desafio vocês precisarão:
- Listar os pedidos do `orders.js`
- Botões para filtrar os pedidos por
  - Status
  - Método de pagamento
  - Remover filtro

Na listagem de pedidos deve haver um botão que abrirá o filtro, ao selecionar um filtro volta para a listagem de pedidos apenas com os itens filtrados. 

Devem mostrar os dados na tela:
- Quantidade total dos pedidos (altera quando filtra)
- Total monetário ($) dos pedidos

Para se destacar nesse desafio, faça um filtro de **data de criação**. Para mostrar apenas pedidos entre uma data inicial e uma data final.

### Inicialização:
- De um Fork no repositório: https://gitlab.com/acct.fateclab/turma-1-sem-2019/06-basico-do-react-native;
- Agora você deve clonar o repositório localmente, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com HTTPS: `git clone url-copiada`;
- Agora dentro da pasta clonada, clique com o botão direito do mouse e clique em "Git Bash Here" para entrar na pasta pelo terminal;
- Para iniciar o servidor de desenvolvimento não se esqueça de usar `npm run start` e escanear o QRCode no aplicativo Expo para que o App continue sendo buildado sempre que atualizar os arquivos;

### Entrega: 
- Faça commits para cada forma diferente de exibição do texto;
- Assim que terminar dê `git push origin seu-nome/basico-do-react-native`, crie um Merge Request na interface do GitLab, acesse o menu "Merge Requests" e crie um, configure o "Target Branch" para o repositório original para que seu App seja avaliado e revisado e para que possamos te dar um feedback;
- O nome do Merge Request deve ser o seu nome completo.